# BFTest 

Around 1.2 K lines of code in  18 files ( Files in the thirdparty libs, XIBs and assets are excluded )

###Features

* Authenticate with Google and get API token for Google APIs

* Save token as secured data in the Keychain 

* Download calendar events as JSON for the logged in user by using the saved token.

* Parse the JSON and extract the required data(Email, Start Date, End Date) and save to Userdefaults for offline use

As the downloded data/JSON has the complex levels, I have not created the model struct to support entire properties. I have just extracted three properties(email, start date , end date) from the JSON using Dictionary and created the event object. 

A model struct with codable protocol should be used if we need to have all the values in the JSON to the model object, and the parsing will be automatic. 


* Display the event list in the Table view [ UI is not that great, I think thats ok for the test assignment as nothing mentioned about the UI quality(Look and feel) in the document. I tried to make good UI in the limited time ]

* There are two filter options(by the date and the string contained in the email address).

* Search-bar for email flter , calendar icon for date filter and  X button to clear all filters.

* User can signout from the account using signin/sign-out button

###Technical Description

Sign-in is implemented using GoogleSignIn cocoapod; So, please execute 'pod update' command from the terminal and open the xcode workspace.

I have not used story-board to develop this app, instead I just used XIBs and managed the screen navigation programatically. The intention was to exhibit the code which handles the navigation logic.

#### Explanation about the source code  
There are 18 files included in the source , following are the brief details.

####Non-UI
AppController.swift -

 Sigletone for cordinating the application screens

Authenticater.swift -
This file consisit of following cusntructs

enum AuthError - 

enum AuthenticatorType - Authenticator type for creating Authenticator object

protocol Authenticator - Authenticator has to implement this protocol

class AuthenticatorFactory - Factory for creating Authenticator object as per the give type
 
GAuthenticator.swift - 	Concrete class for Google Authenticator

GCalendarManager.swift - Gogole calendar api implemented here

NetworkManager.swift - NSURLSession implemented here

KeyChain.swift -  Saving data in the key chain is here

Models.swift - 	User and Event models are here

####UI

#####Screens

LoginViewController.swift - Screen for displatying the list of events

CalendarViewController.swift - Screen for displaying Google calendar in webview

#####Custom
BButton.swift - Underlined button , Cancel button (Button with custom drawing)

EventCell.swift - Table view cell for event

DatePickerContainer.swift - Container view for Date picker

[Class Diagram](./BFTest.png)

####Unit test

ModelsTests.swift -

NetworkManagerTests.swift -

KeyChainTests.swift -

As the unit test is not mentioned in the requirement document other classes are not unit-tested.

I know the coverage percentage is insufficient for the production code , here just wanted to exhibit the unit-test approach.

[Test Coverage Report](./BFTest-Coverage.png)


