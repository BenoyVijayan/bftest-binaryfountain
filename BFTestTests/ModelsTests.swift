//
//  ModelsTests.swift
//  BFTestTests
//
//  Created by Binoy Vijayan on 7/22/18.
//  Copyright © 2018 Binoy Vijayan. All rights reserved.
//

import XCTest

class ModelsTests: XCTestCase {
    
    var eventDict1:[String:String]?
    var eventDict2:[String:String]?
    override func setUp() {
        super.setUp()
        eventDict1 = ["email":"mail@example.com" , "start":"2018-07-22 14:11:53" , "end":"2018-07-22 15:11:53" ,"link":"https://www.google.com/calendar/event?eid=YjZidm1vM3Y5YXVlZnFwb3FtbXBhcjVpb2cgYmVub3kuYXBwbGVAbQ" ]
        eventDict2 = ["email":"mail1@example.com" , "start":"2018-07-22 14:11:54" , "end":"2018-07-22 15:11:54","link":"https://www.google.com/calendar/event?eid=YjZidm1vM3Y5YXVlZnFwb3FtbXBhcjVpb2cgYmVub3kuYXBwbGVAbQ"]
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        
        eventDict1 = nil
        eventDict2 = nil
    }
    
    func testUserModel(){
        let user1 = User(name: "Ram", email: "ram@gmail.com")
        let user2 = User(dictionary: ["name":"Ram" , "email": "ram@gmail.com"])
        XCTAssertEqual(user1.name, user2.name)
        XCTAssertEqual(user1.email, user2.email)
        XCTAssertEqual(user1.dictionary, user2.dictionary)
        
        user1.save()
        guard let user3 = User.user else{
            XCTAssertFalse(true)
            return
        }
        XCTAssertEqual(user1.name, user3.name)
        XCTAssertEqual(user1.email, user3.email)
        XCTAssertEqual(user1.dictionary, user3.dictionary)
        XCTAssertEqual(user3.name, user3["name"])
        XCTAssertEqual(user3.email, user3["email"])
        User.remove()
        XCTAssertNil(User.user)
    }
    
    func testEventModel() {
        let event1 = Event(dictionary: eventDict1!)
        let event2 = Event(dictionary: eventDict1!)
        
        XCTAssertEqual(event1.email, event2.email)
        XCTAssertEqual(event1.start, event2.start)
        XCTAssertEqual(event1.end, event2.end)
        XCTAssertEqual(event1.dictionary, event2.dictionary)
        
        let event3 = Event(dictionary: eventDict2!)
        XCTAssertNotEqual(event1.email, event3.email,"Events  are not equal")
        XCTAssertNotEqual(event1.start, event3.start,"Events are not equal")
        XCTAssertNotEqual(event1.end, event3.end,"Events are not equal")
        XCTAssertNotEqual(event1.dictionary, event3.dictionary ,"Events are not equal" )
        XCTAssertNotEqual(event2.dictionary, event3.dictionary ,"Events are not equal" )
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
