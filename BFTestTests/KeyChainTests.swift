//
//  KeyChainTests.swift
//  BFTestTests
//
//  Created by Binoy Vijayan on 7/23/18.
//  Copyright © 2018 Binoy Vijayan. All rights reserved.
//

import XCTest

class KeyChainTests: XCTestCase {
    
    let strData = "PASSWORD"
    var key:String?
    var data:Data?
    
    override func setUp() {
        super.setUp()
        key = "passwordKey"
        data = strData.data(using: .utf8, allowLossyConversion: false)
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        
        _ = KeyChain.delete(key: key!)
        key = nil
        data = nil
    }
    
    func testSave(){
        let status = KeyChain.save(key: key!, data: data!)
        XCTAssertTrue(status == 0)
        let loadedData = KeyChain.load(key: key!)
        XCTAssertTrue(data == loadedData)
    }
    
    func testLoad(){
        let status = KeyChain.save(key: key!, data: data!)
        XCTAssertTrue(status == 0)
        let loadedData = KeyChain.load(key: key!)
        XCTAssertTrue(data == loadedData)
    }
    
    func testDelete(){
        var status = KeyChain.save(key: key!, data: data!)
        XCTAssertTrue(status == 0)
        status = KeyChain.delete(key: key!)
        XCTAssertTrue(status == 0)
        let loadedData = KeyChain.load(key: key!)
        XCTAssertNil(loadedData)
    }
    
    func testKeyChain() {
        
        var status = KeyChain.save(key: key!, data: data!)
        XCTAssertTrue(status == 0)
        
        var loadedData = KeyChain.load(key: key!)
        XCTAssertTrue(data == loadedData)
        
        let actualStrData = String(data: loadedData!, encoding: .utf8)
        XCTAssertTrue(strData == actualStrData)
        
        status = KeyChain.delete(key: key!)
        XCTAssertTrue(status == 0)
        
        loadedData = KeyChain.load(key: key!)
        XCTAssertNil(loadedData)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
