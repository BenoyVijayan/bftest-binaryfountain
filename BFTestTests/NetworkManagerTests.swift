//
//  NetworkManagerTests.swift
//  BFTestTests
//
//  Created by Binoy Vijayan on 7/22/18.
//  Copyright © 2018 Binoy Vijayan. All rights reserved.
//

import XCTest

class NetworkManagerTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testNetworkLibForType(){
        
        let nativeManager = NetworkLibFactory.networkLibForType(.native)
        XCTAssertTrue(nativeManager is NativeNetworkManager)
        
        let alamofireManager =  NetworkLibFactory.networkLibForType(.alamofire)
        XCTAssertTrue(alamofireManager is AlamofireNetworkManager)
    }
    
    func testDownlaodData(){
        let expectation = XCTestExpectation(description: "Download JSON")
        
        let url = URL(string: "https://jsonplaceholder.typicode.com/posts/1")!
        let nativeManager = NetworkLibFactory.networkLibForType(.native)
        nativeManager.downloadData(url: url, completion: {(data, error) in
            XCTAssertNotNil(data, "No data was downloaded.")
            expectation.fulfill()
        })
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testDownloadData_Almofire(){
        
        let alamofireManager = NetworkLibFactory.networkLibForType(.alamofire)
        let url = URL(string: "https://jsonplaceholder.typicode.com/posts/1")!
        let expectation = XCTestExpectation(description: "Download JSON")
        alamofireManager.downloadData(url: url, completion: {(data, error) in
            XCTAssertNil(error)
            expectation.fulfill()
        })
        wait(for: [expectation], timeout: 1.0)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
