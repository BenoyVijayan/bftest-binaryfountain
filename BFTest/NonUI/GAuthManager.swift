//
//  GAuthManager.swift
//  BFTest
//
//  Created by Binoy Vijayan on 7/21/18.
//  Copyright © 2018 Binoy Vijayan. All rights reserved.
//

import Foundation
import GoogleSignIn

class GAuthenticator: NSObject, Authenticator{
   
    private var didCompleteSignIn:((User?,AuthError?)->())?
    private var didCompleteSignOut:((AuthError?)->())?
    private var didTokenRefresh:()?
    private var currentUser:User?
    
    private func saveTokensLocally( _ tokens:[String:String]){
        
        guard let jsonData = try? JSONSerialization.data(withJSONObject: tokens, options: .prettyPrinted) else{
            return
        }
        
       let status = KeyChain.save(key: AppController.instance.appUUID, data: jsonData)
       print("\(status)")
    }
    
    override  init(){
        super.init()
        GIDSignIn.sharedInstance().clientID = Constants.Google.authClientId
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self 
        GIDSignIn.sharedInstance().scopes = Constants.Google.apiScope
    }
    
    var isAuthenticated:Bool{
        get{
            return nil != self.getSavedTokens()
        }
    }
    
    /*
        This will initiate signin sequence of google signin
     */
    func showSignIn(completion:@escaping (User?, AuthError?)->() ){
        didCompleteSignIn = completion
        GIDSignIn.sharedInstance().signIn()
    }
    
    /*
      Signout will remove the exsting objects related to the user
     */
    func signOut(completion: @escaping ((AuthError?)->())  ){        
        GIDSignIn.sharedInstance().signOut()
       let status = KeyChain.delete(key: AppController.instance.appUUID)
        print("\(status)")
        UserDefaults.standard.removeObject(forKey: Constants.App.eventsSavedKey)
        didCompleteSignOut = completion
        didCompleteSignOut!(nil)
    }
    
    /*
        Extracting the saved token from User dfaults
     */
    func getSavedTokens()->[String:String]?{
        
        guard let data = KeyChain.load(key: AppController.instance.appUUID) else{
            return nil
        }
        
        guard let jsonDict = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String : String] else{
            return nil
        }

        return jsonDict
    }
    
    /*
        This will check if the existing token expired before maping the API call
     */
    var isTokenExpired:Bool{
        get{
            guard let auth = GIDSignIn.sharedInstance()?.currentUser?.authentication else{
                return true
            }
            if auth.accessTokenExpirationDate > Date() {
                return false
            }
            return true
        }
    }
    
    /*
        This will refresh token if the existing token expired
     */
    func refreshToken(completion: @escaping () -> ()) {
        GIDSignIn.sharedInstance()?.currentUser?.authentication?.refreshTokens(handler:{(auth , error) in
            guard let auth = auth else{ return }
            let tokenDict = [Constants.App.accessTokenKey:auth.accessToken! ,
                             Constants.App.refreshTokenKey :auth.refreshToken! ]
            
            self.saveTokensLocally(tokenDict)
            completion()
        })
    }
    
    /*
        Returns current logged in user's info from either memory or userdefaults
     */
    func getCurrentUser()->User? {
     
        if currentUser == nil{
            currentUser = User.user
        }
        return currentUser
    }
}

extension GAuthenticator: GIDSignInDelegate{
   
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if error != nil {
            didCompleteSignIn!(nil,.invalidCredentials)
        } else {
            currentUser = User(name: user.profile.name!, email: user.profile.email)
            currentUser?.save()
            
            let tokenDict = [Constants.App.accessTokenKey:user.authentication.accessToken! ,
                             Constants.App.refreshTokenKey :user.authentication.refreshToken! ]
            
            self.saveTokensLocally(tokenDict)
            didCompleteSignIn!(currentUser,nil)
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        
    }
    
}

extension GAuthenticator:GIDSignInUIDelegate{
    
    func sign(inWillDispatch signIn: GIDSignIn, error: Error) {
        
    }
    
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!,present viewController: UIViewController!) {
        
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        
    }
}
