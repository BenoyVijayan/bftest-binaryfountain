//
//  GCalendarManager.swift
//  BFTest
//
//  Created by Binoy Vijayan on 7/21/18.
//  Copyright © 2018 Binoy Vijayan. All rights reserved.
//

import Foundation


/* Error for network call */
enum CalendarError:Error{
    case authenticationRequired
    case noFutureEvents
    case invalidCalendar
    case serverError
}

class GCalendarManager{
    
    private let networkManger:Networkable = NetworkLibFactory.networkLibForType(.native)
    private var authenticator:Authenticator?
    
    init(authenticator:Authenticator){
        self.authenticator = authenticator
    }
    
    /* Store events in user defaults for offline use */
    private func saveEvents( _ events: [[String:String]] ){
        UserDefaults.standard.set(events, forKey: Constants.App.eventsSavedKey)
    }
    
    /* Returns saved events if already stored otherwise nil */
    func getSavedEvents()->[Event]?{
       
        guard let dictArr:[[String:String]] =  (UserDefaults.standard.object(forKey: Constants.App.eventsSavedKey) as? [[String:String]]) else{
            return nil
        }
        let retEvents:[Event] = dictArr.map{ Event(dictionary: $0 ) }
        return retEvents
    }
    
    /*
        Immediately returns saved events if available, then downloading events from google calendar
     */
    func fetchALlEvents( completion: @escaping ([Event]? ,CalendarError? )->()){
        
        let savedEvents = getSavedEvents()
        completion(savedEvents, nil)
        
        func extractEvents( _ dict:[String:Any]){
            let items = dict[Constants.App.itemsKey] as! [NSDictionary]
            var events:[Event] = [Event]()
            var eventDicts:[[String:String]] = [[String:String]]()
            for item in items{
                
                let email = item.value(forKeyPath: Constants.App.orgnizerEmailKeyPath) as! String
                let start = item.value(forKeyPath: Constants.App.startDateTimeKeyPath) as! String
                let end = item.value(forKeyPath: Constants.App.endDateTimeKeyPath) as! String
                let link = item.value(forKeyPath: Constants.App.htmlLinkKey) as! String
    
                let dict =  [Constants.App.emailKey:email, Constants.App.startKey:start , Constants.App.endKey:end , Constants.App.linkKey:link ]
                let event = Event(dictionary:dict)
                eventDicts.append(dict)
                events.append(event)
            }
            saveEvents(eventDicts)
            completion(events,nil)
        }
        
        func initiateDownload(){
            if let accessTokens = authenticator?.getSavedTokens() ,
                let cUser = authenticator?.getCurrentUser()  {
                let token = accessTokens[Constants.App.accessTokenKey]
                let urlStr = Constants.Google.urlFirstPart + cUser.email! + Constants.Google.urlSecondPart + token!
                guard let url = URL(string: urlStr) else{ return }
                networkManger.downloadData(url: url, completion: { (data , error) in
                    if data != nil{
                    
                        guard let dict = try? JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any] else{

                            return
                        }
                        extractEvents(dict!)
                    }
                })
            }
        }
        
        if !authenticator!.isAuthenticated{
            completion(nil, .authenticationRequired)
            
            return
        }
        
        if authenticator!.isTokenExpired{
            authenticator!.refreshToken( completion: {
                initiateDownload()
            })
            
            return
        }else{
           initiateDownload()
        }
    }
}
