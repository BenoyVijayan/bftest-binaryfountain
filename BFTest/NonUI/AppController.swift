//
//  AppController.swift
//  BFTest
//
//  Created by Binoy Vijayan on 7/19/18.
//  Copyright © 2018 Binoy Vijayan. All rights reserved.
//

import Foundation
import UIKit

class AppController{
  
    /* Private members */
    private let window:UIWindow = UIWindow(frame: UIScreen.main.bounds)
    private var navigationController:UINavigationController?
    private let loginScreen:LoginViewController =  LoginViewController.instance
    private var calendarScreen:CalendarViewController?
    private init(){
        window.makeKeyAndVisible()
    }
    
    /* Public members */
    public static let instance:AppController = AppController()
    
    var appUUID:String{
        get{
            var uuidString:String? = UserDefaults.standard.string(forKey: Constants.App.appUUIDKey)
            if uuidString == nil {
                uuidString = UUID().uuidString
                UserDefaults.standard.set(uuidString, forKey: Constants.App.appUUIDKey)
            }
            return uuidString!
        }
    }
    
    func showLoginScreen(){
        navigationController = UINavigationController(rootViewController: loginScreen)
        self.navigationController!.navigationBar.isHidden = true
        self.window.rootViewController = self.navigationController
    }
    
    func showCalendarScreenWith(url:String){
        if calendarScreen == nil{
            calendarScreen = CalendarViewController.instance
        }
        self.navigationController!.pushViewController(calendarScreen!, animated: true)
        calendarScreen!.urlString = url
    }
    
}
