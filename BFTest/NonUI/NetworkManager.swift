//
//  NetworkManager.swift
//  BFTest
//
//  Created by Binoy Vijayan on 7/21/18.
//  Copyright © 2018 Binoy Vijayan. All rights reserved.
//

import Foundation

/* Error for network call */
enum NetworkError:Error{
    case inavlidUrl
    case serverError
    case invalidData
}

/* Error for network call */
enum NetworkLibType {
    case native
    case alamofire
}

protocol Networkable{
    func downloadData(url:URL , completion : @escaping ( Data? , NetworkError? ) ->())
}

/* Factory for creating NetworkManager based on the type*/
class NetworkLibFactory{
    
    static func networkLibForType(_ type:NetworkLibType)->Networkable{
        
        switch type {
        case .native:
            return NativeNetworkManager()
        case .alamofire :
            return AlamofireNetworkManager()
        }
    }
}

/* NetworkManger with iOS SDK apis */
class NativeNetworkManager:Networkable{
    
    func downloadData(url:URL , completion : @escaping ( Data? , NetworkError? ) ->()){
        
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig)
        
        let dataTask = session.dataTask(with: url, completionHandler: {(data,urlRespnse,error) in
            if error == nil {
                completion(data,nil)
            }else{
                completion(nil, .serverError)
            }
        })
        dataTask.resume()
    }
}

/*  NetworkManger with Alamofire framework  */
class AlamofireNetworkManager:Networkable{
    
    func downloadData(url:URL , completion : @escaping ( Data? , NetworkError? ) ->()){
        
        completion(nil , nil)
        // Provide implementation if we use Alamofire
    }
}


