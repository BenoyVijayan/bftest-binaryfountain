//
//  Authenticator.swift
//  BFTest
//
//  Created by Binoy Vijayan on 7/21/18.
//  Copyright © 2018 Binoy Vijayan. All rights reserved.
//

import Foundation

enum AuthError:Error{
    case invalidCredentials
    case couldNotSignOut
}

enum AuthenticatorType{
    
    case google
    case facebook
    case twiter
    case linkedIn
}

protocol Authenticator {    
    func showSignIn(completion:@escaping (User?, AuthError?)->() )
    func signOut(completion: @escaping ((AuthError?)->()) )
    func getSavedTokens()->[String:String]?
    func refreshToken(completion:@escaping ()->())
    func getCurrentUser()->User?

    var isAuthenticated:Bool{ get }
    var isTokenExpired:Bool{ get }
}

class AuthenticatorFactory{
    
    static func authenticatorFor(type:AuthenticatorType )->Authenticator{
        
        switch type {
        case .google , .facebook ,.linkedIn ,.twiter:
            return GAuthenticator()
        }
    }
}
