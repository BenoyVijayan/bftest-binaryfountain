//
//  Constants.swift
//  BFTest
//
//  Created by Binoy Vijayan on 7/19/18.
//  Copyright © 2018 Binoy Vijayan. All rights reserved.
//

import Foundation
import UIKit

struct Constants{
    
    /* Constants specific for Google */
    struct Google {
        static let authClientId = "702935501132-rb360mmfs1fm8pv7croj54u1jshggq5d.apps.googleusercontent.com"
        static let apiScope = ["https://www.googleapis.com/auth/calendar",
                               "https://www.googleapis.com/auth/calendar.readonly"] as [Any]
        static let urlFirstPart = "https://www.googleapis.com/calendar/v3/calendars/"
        static let urlSecondPart = "/events?access_token="
    }
    
    /* Constants specific App */
    struct App{
        static let tokenSavedKey = "Tokens"
        static let accessTokenKey = "AccessToken"
        static let refreshTokenKey = "RefreshToken"
        static let userSavedKey = "User"
        static let eventsSavedKey = "Events"
        static let eventCellId = "EventCell"
        static let loginScreenNibName = "LoginViewController"
        static let calendarScreenNibName = "CalendarViewController"
        static let eventCellHeight = 80.0
        static let appUUIDKey = "AppUUID"
        static let itemsKey = "items"
        static let htmlLinkKey = "htmlLink"
        static let nameKey = "name"
        static let linkKey = "link"
        static let emailKey = "email"
        static let startKey = "start"
        static let endKey = "end"
        static let orgnizerEmailKeyPath = "organizer.email"
        static let startDateTimeKeyPath = "start.dateTime"
        static let endDateTimeKeyPath = "end.dateTime"
        static let dateFormat = "yyyy-MM-dd"
        static let webBlankUrl = "about:blank"
        static let cellOddColor =  UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 80.0/255.0, alpha: 30.0/255.0)
        static let cellEvenColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 80.0/255.0, alpha: 20.0/255.0)

        /*
            - Strings for display
            - Should be moved to string resources to support localisation
         */        
        static let signInText = "Sign in"
        static let signOutText = "Sign out"
        static let welcomeText = "Welcome !"
        static let titleText = "Events for"
        static let emailText = "Email :"
        static let startText = "Start :"
        static let endText = "End :"
        static let allEventsText = "All Events"
        static let eventsInText = "Events in"
    }
}
