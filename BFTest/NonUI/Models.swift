//
//  Models.swift
//  BFTest
//
//  Created by Binoy Vijayan on 7/21/18.
//  Copyright © 2018 Binoy Vijayan. All rights reserved.
//

import Foundation

struct User{
    let name:String?
    let email:String?
    
    init( name:String , email:String){
        self.name = name
        self.email = email
    }
    
    init(dictionary:[String:String]){
        self.name = dictionary[Constants.App.nameKey]
        self.email = dictionary[Constants.App.emailKey]
    }
    
    var dictionary:[String:String]?{
        get{
            let dict = [Constants.App.nameKey:self.name! , Constants.App.emailKey:self.email! ]
            return dict
        }
    }
    
    func save(){
        guard let dict = self.dictionary else{ return }
        UserDefaults.standard.set(dict, forKey: Constants.App.userSavedKey)
    }
    
    static var user:User?{
        get{
            guard let dict =  UserDefaults.standard.object(forKey: Constants.App.userSavedKey) else{
                return nil
            }
            let usr = User(dictionary: dict as! [String : String])
            return usr
        }
    }
    
    static func remove(){
        UserDefaults.standard.removeObject(forKey: Constants.App.userSavedKey)
    }
    
    subscript(key:String)->String?{
        get{
            let dict = self.dictionary
            return dict?[key]
        }
    }
}

struct Event{

    let email:String?
    let start:String?
    let end:String?
    let link:String?
    
    init(dictionary:[String:String]){
        self.email = dictionary[Constants.App.emailKey]
        self.start = dictionary[Constants.App.startKey]
        self.end = dictionary[Constants.App.endKey]
        self.link = dictionary[Constants.App.linkKey]
    }
    
    var dictionary:[String:String]?{
        get{
            let dict = [Constants.App.emailKey:self.email!,        Constants.App.startKey:self.start! , Constants.App.endKey:self.end!,
                Constants.App.linkKey:self.link!]
            
            return dict
        }
    }
}

