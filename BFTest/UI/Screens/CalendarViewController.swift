//
//  CalendarViewController.swift
//  BFTest
//
//  Created by Binoy Vijayan on 7/22/18.
//  Copyright © 2018 Binoy Vijayan. All rights reserved.
//

import UIKit
import WebKit

class CalendarViewController: UIViewController {

    @IBOutlet var webView: WKWebView?
    @IBOutlet var activityView: UIActivityIndicatorView!
    
    var urlString:String = ""{
        didSet{
            webView?.reload()
        }
    }
    
    static var instance:CalendarViewController{
        get{
            return CalendarViewController(nibName: Constants.App.calendarScreenNibName, bundle: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadUrl()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didTapBackButton(_ sender:AnyObject){
        webView?.load(URLRequest(url: URL(string:Constants.App.webBlankUrl)!))
        self.navigationController?.popViewController(animated: true)
    }
    
    private func loadUrl(){
        guard let url = URL(string: urlString) else{
            return
        }
        let request = URLRequest(url: url)
        webView?.load(request)
        webView?.navigationDelegate = self
    }
    
    private func startActivity(){
        activityView.isHidden = false
        activityView.startAnimating()
    }
    
    private func stopActivity(){
        activityView.isHidden = true
        activityView.stopAnimating()
    }
 
}

extension CalendarViewController: WKNavigationDelegate{
    
    public func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!){
        startActivity()
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!){
        stopActivity()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error){
        stopActivity()
    }

}
