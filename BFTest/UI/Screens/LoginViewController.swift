//
//  LoginViewController.swift
//  BFTest
//
//  Created by Binoy Vijayan on 7/19/18.
//  Copyright © 2018 Binoy Vijayan. All rights reserved.
//

import UIKit
import GoogleSignIn

class LoginViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var eventTable: UITableView!
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    @IBOutlet weak var authButton: FlipButton!
    @IBOutlet weak var datePickerView: DatePickerContainer!
    @IBOutlet weak var datePickerButton: UIButton!
    @IBOutlet weak var dateLabel: UILabel!
    
    private let gAuthenticator:Authenticator = AuthenticatorFactory.authenticatorFor(type: .google)
    private var calendarManager:GCalendarManager?
    private var events:[Event]?
    private var filteredEvents:[Event]?
    
    static var instance:LoginViewController{
        get{
            return LoginViewController(nibName: Constants.App.loginScreenNibName, bundle: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if gAuthenticator.isAuthenticated == true {
            authButton.flipState = .back
            fetchAllEvents()
            self.titleLabel.text = "\(Constants.App.titleText) \(gAuthenticator.getCurrentUser()?.name ?? "")"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        calendarManager = GCalendarManager(authenticator: gAuthenticator)
        let nib = UINib(nibName: Constants.App.eventCellId, bundle: Bundle(for: EventCell.self))
        eventTable.register(nib, forCellReuseIdentifier: Constants.App.eventCellId)
        
        datePickerView.didSelectDate = { (dateStr) in
            self.filteredEvents = self.events?.filter{
                let  sourceTxt = ($0.start?.uppercased())!
                let destText = dateStr.uppercased()
                return sourceTxt.contains(destText)
            }
        
            self.eventTable.reloadData()
            self.dateLabel.text = "\(Constants.App.eventsInText) \(dateStr)"
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private func fetchAllEvents(){
        activityView.isHidden = false
        activityView.startAnimating()
        self.dateLabel.text = Constants.App.allEventsText
        calendarManager?.fetchALlEvents(completion:{(events, error) in
            self.events = events
            self.filteredEvents = events
            DispatchQueue.main.async {
                self.eventTable.reloadData()
                self.activityView.isHidden = true
                self.activityView.stopAnimating()
                
            }
        } )
    }

    @IBAction func didTapAuthButton(_ sender: FlipButton) {
        searchBar.resignFirstResponder()
        sender.startActivity()
        if sender.flipState == .face{
            gAuthenticator.showSignIn(completion: { (user , error) in
                DispatchQueue.main.async {
                    if let name = user?.name {
                        self.titleLabel.text = "\(Constants.App.titleText) \(name)"
                        self.fetchAllEvents()
                    }
                    stopActivity()
                }
            })
        }else if sender.flipState == .back{
            gAuthenticator.signOut(completion: { (error) in
                DispatchQueue.main.async {
                    stopActivity()
                    self.titleLabel.text = Constants.App.welcomeText
                    self.filteredEvents?.removeAll()
                    self.eventTable.reloadData()
                    self.datePickerView.hide()
                    self.searchBar.resignFirstResponder()
                }
            })
        }
        
        func stopActivity(){
            sender.stopActivity()
            sender.flip()
        }
    }
    
    @IBAction func didTapPickDateButton(_ sender: UIButton){
        searchBar.text = ""
        searchBar.resignFirstResponder()
        datePickerView.show()
    }
    
    @IBAction func didTapClearFilterButton(_ sender: CancelButton){
        filteredEvents = events
        eventTable.reloadData()
        searchBar.resignFirstResponder()
        datePickerView.hide()
        dateLabel.text = Constants.App.allEventsText
        searchBar.text = ""
    }
}

extension LoginViewController:UISearchBarDelegate{
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        datePickerView.hide()
        
        return true
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        
        searchBar.resignFirstResponder()
        if searchBar.text?.count == 0 || searchBar.text == " "{
            filteredEvents = events
        }else{
            filteredEvents = events?.filter{
                let  sourceTxt = ($0.email?.uppercased())!
                let destText = (searchBar.text?.uppercased())!
                return  sourceTxt.contains(destText)
            }
        }
        self.dateLabel.text = Constants.App.allEventsText
        eventTable.reloadData()
    }
}

extension LoginViewController:UITableViewDataSource{
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
     
        return filteredEvents?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.App.eventCellId)
        cell?.accessoryType = .disclosureIndicator
        cell?.backgroundColor = indexPath.row % 2 == 0 ? Constants.App.cellEvenColor : Constants.App.cellOddColor
        let event = filteredEvents?[indexPath.row]
        guard let customCell = cell as? EventCell else {
            
            return cell!
        }
        customCell.event = event
        
        return customCell
    }
}

extension LoginViewController:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        
        return CGFloat(Constants.App.eventCellHeight)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        guard let link = filteredEvents?[indexPath.row].link else{
            tableView.deselectRow(at: indexPath, animated: true)
            
            return
        }
        AppController.instance.showCalendarScreenWith(url: link)
    }
}
