//
//  EventCell.swift
//  BFTest
//
//  Created by Binoy Vijayan on 7/21/18.
//  Copyright © 2018 Binoy Vijayan. All rights reserved.
//

import UIKit

class EventCell: UITableViewCell {

    @IBOutlet weak var emailLabel:UILabel!
    @IBOutlet weak var startLabel:UILabel!
    @IBOutlet weak var endLabel:UILabel!
    
    var event:Event?{
        didSet{
            emailLabel.text = "\(Constants.App.emailText) \(event?.email ?? "")"
            startLabel.text = "\(Constants.App.startText) \(event?.start ?? "")"
            endLabel.text = "\(Constants.App.endText) \(event?.end ?? "")"
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
