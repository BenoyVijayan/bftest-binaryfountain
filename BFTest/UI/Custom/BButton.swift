//
//  BButton.swift
//  BFTest
//
//  Created by Binoy Vijayan on 7/21/18.
//  Copyright © 2018 Binoy Vijayan. All rights reserved.
//

import UIKit
enum FlipState{
    case face
    case back
}

@IBDesignable class FlipButton: UIButton {

    @IBInspectable var firstLabel:String = Constants.App.signInText
    @IBInspectable var secondLabel:String = Constants.App.signOutText
    
    /* Modify under-line as per the title */
    override func setTitle(_ title: String?, for state: UIControlState) {
        super.setTitle(title, for: state)
        drawUnderLine()
    }
    
    /* Setting the face of the button and setting repective title */
    var flipState:FlipState = .face{
        didSet{
            let title = self.flipState == .face ? self.firstLabel:self.secondLabel
            self.setTitle(title, for: .normal)
        }
    }
    
    private let activityView:UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    /* Adding activity indicator view as subview */
    private func initialize(){
        self.activityView.color = UIColor.white
        self.addSubview(self.activityView)
        self.bringSubview(toFront: self.activityView)
        self.activityView.isHidden = true
        self.activityView.frame = CGRect(x:0,y:0,width:30,height:30)
        self.activityView.center =  CGPoint(x:self.frame.size.width / 2 , y : self.frame.size.height / 2 )
    }
    
    /* Doing flip animation while changing the title */
    func flip(){
        flipState =  flipState == .face ? .back : .face
        UIView.transition(with: self, duration: 0.5, options:.transitionFlipFromLeft , animations: {
            let title = self.flipState == .face ? self.firstLabel:self.secondLabel
            self.setTitle(title, for: .normal)
        }, completion:{(done) in
            
        } )
    }

    /* Drawing underline based on title length */
    private func drawUnderLine(){
        
        DispatchQueue.main.async {
            
            let textbound = self.titleLabel?.font.boundingBoxForText(text: (self.titleLabel?.text)!)
            let btnWidth = self.bounds.size.width
            let btnHeight = self.bounds.size.height
            let txtWidth = textbound?.size.width
            let txtHeight = textbound?.size.height
            let txtX:CGFloat =  (btnWidth - txtWidth!) / 2
            let txtY:CGFloat = (btnHeight - txtHeight!) / 2
            let bPoint = CGPoint(x : txtX , y : txtY + txtHeight!)
            let ePoint = CGPoint(x : txtX +  txtWidth! , y : txtY + txtHeight!)
            let line:CAShapeLayer = CAShapeLayer()
            let path:UIBezierPath = UIBezierPath()
            path.move(to: bPoint)
            path.addLine(to: ePoint)
            line.path = path.cgPath
            line.fillColor = UIColor.clear.cgColor
            line.lineWidth = 1.0
            line.opacity = 1.0
            line.strokeColor = self.titleLabel?.textColor.cgColor
            self.layer.addSublayer(line)
        }
    }
    
    func startActivity(){
        activityView.isHidden = false
        activityView.startAnimating()
    }
    
    func stopActivity(){
        activityView.stopAnimating()
        activityView.isHidden = true
    }
}


@IBDesignable class CancelButton: UIButton {
    
    @IBInspectable var lineWidth:CGFloat = 1.0
    @IBInspectable var lineColor:UIColor = UIColor.white
    
    override func draw(_ rect: CGRect) {
        
        let margin = rect.width / 3.2
        let pt1 = CGPoint(x: margin , y: margin)
        let pt2 = CGPoint(x: rect.size.width -  margin , y: rect.size.height - margin)
        let pt3 = CGPoint(x: rect.size.width - margin , y: margin)
        let pt4 = CGPoint(x: margin , y: rect.size.height - margin)
        
        let line1Path:UIBezierPath = UIBezierPath()
        line1Path.move(to: pt1)
        line1Path.addLine(to: pt2)
        
        line1Path.move(to: pt3)
        line1Path.addLine(to: pt4)
        
        lineColor.setStroke()
        line1Path.lineWidth = lineWidth
        line1Path.stroke()
    }
}

@IBDesignable class BackButton: UIButton {
    
    @IBInspectable var lineWidth:CGFloat = 1.0
    @IBInspectable var lineColor:UIColor = UIColor.white
    
    override func draw(_ rect: CGRect) {
        
        let xMargin = rect.width / 2.5
        let yMargin = rect.height / 3.5
        
        let pt1 = CGPoint(x:rect.size.width - xMargin , y: yMargin)
        let pt2 = CGPoint(x: xMargin , y: rect.size.height / 2)
        let pt3 = CGPoint(x: rect.size.width - xMargin , y: rect.size.height - yMargin)
        
        let linePath:UIBezierPath = UIBezierPath()
        linePath.move(to: pt1)
        linePath.addLine(to: pt2)
        linePath.addLine(to: pt3)
        
        lineColor.setStroke()
        linePath.lineWidth = lineWidth
        linePath.stroke()
    }
}

