//
//  DatePickerContainer.swift
//  BFTest
//
//  Created by Binoy Vijayan on 7/22/18.
//  Copyright © 2018 Binoy Vijayan. All rights reserved.
//

import UIKit

class DatePickerContainer: UIView {

    @IBOutlet weak var bottom:NSLayoutConstraint!
    @IBOutlet weak var datePicker:UIDatePicker!
    
    var didSelectDate:((String)->())?
    func show(){
        
        datePicker.timeZone = TimeZone.current
        
        self.bottom.constant = 0
        UIView.animate(withDuration: 0.5, animations: {
            self.superview?.layoutIfNeeded()
        })
    }
    
    func hide(){
        
        self.bottom.constant = -200
        UIView.animate(withDuration:  0.5, animations: {
            self.superview?.layoutIfNeeded()
        })
        
    }
    
    @IBAction func didTapDoneButton(_ sender:UIButton){
        updateSelectedDate()
        hide()
    }
    
    @IBAction func didSelectDate( _ sender:UIDatePicker){        
       updateSelectedDate()
    }
    
    func updateSelectedDate(){
        
        let formater = DateFormatter()
        formater.dateFormat = Constants.App.dateFormat
        let str = formater.string(from: datePicker.date)
        didSelectDate!(str)
    }
    
}
