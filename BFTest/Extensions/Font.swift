//
//  Font.swift
//  BFTest
//
//  Created by Binoy Vijayan on 7/22/18.
//  Copyright © 2018 Binoy Vijayan. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    
    func boundingBoxForText(text:String)->CGRect{
        
        let constraintRect = CGSize(width: CGFloat.greatestFiniteMagnitude, height: CGFloat.greatestFiniteMagnitude)
        let normalTextAttributes: [NSAttributedStringKey : Any] = [NSAttributedStringKey.font : self]
        let bndbx = text.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: normalTextAttributes, context: nil)
        return bndbx
    }
}
